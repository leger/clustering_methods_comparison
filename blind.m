% This script run clustering method for a unkonown number of groups
% (blind). The results are merged with previous result.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


allres = calc_blind_init(parameters);

% sc
allres{1}.Bw=parcellfun(np,@(n) sc(n.A,0,1,1),networks,'UniformOutput',false);
allres{2}.Bw=parcellfun(np,@(n) sc(n.A,0,2,0),networks,'UniformOutput',false);

allres{1}.Bb=parcellfun(np,@(n) sc(1*(n.A>0),0,1,1),networks,'UniformOutput',false);
allres{2}.Bb=parcellfun(np,@(n) sc(1*(n.A>0),0,2,0),networks,'UniformOutput',false);

% mcl
allres{3}.Bw=parcellfun(np,@(n) mcl(n.A,1,0),networks,'UniformOutput',false);
allres{4}.Bw=parcellfun(np,@(n) mcl(n.A,.1,0),networks,'UniformOutput',false);

allres{3}.Bb=parcellfun(np,@(n) mcl(1*(n.A>0),1,0),networks,'UniformOutput',false);
allres{4}.Bb=parcellfun(np,@(n) mcl(1*(n.A>0),.1,0),networks,'UniformOutput',false);

% eb
allres{5}.Bb=parcellfun(np,@(n) eb(n,1,0),networks,'UniformOutput',false);
allres{5}.Bw=parcellfun(np,@(n) eb(n,0,0),networks,'UniformOutput',false);

% mod
allres{6}.Bw=parcellfun(np,@(n) mod2(n.A,0),networks,'UniformOutput',false);
allres{6}.Bb=parcellfun(np,@(n) mod2(1*(n.A>0),0),networks,'UniformOutput',false);

% sbm
allres{7}.Bw=parcellfun(np,@(n) sbmP(n,0,0),networks,'UniformOutput',false);
allres{7}.Bb=parcellfun(np,@(n) sbmP(n,1,0),networks,'UniformOutput',false);

save -z netdir/blind_results
