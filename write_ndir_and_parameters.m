% This script write parameters object.


% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 



vpcomp=linspace(.1,.9,10);
mpcomp=.6;
vpnest=linspace(.1,.9,10);
mpnest=.5;
vg=[2 4 8 16];
mg=4;

vks = exp(linspace(log(56),log(1e5),10));
vkr = exp(linspace(log(1),log(46),10));
vkl = exp(linspace(log(.41),log(16.2),10));
vkw = exp(linspace(log(1.25),log(270),10));

moyg=@(x,y) exp((log(x)+log(y))/2);

mks = moyg(38,36e3);
mkr = moyg(1,12);
mkl = moyg(.41,4.75);
mkw = moyg(1.5,48);

vrep=1:100;

i=0;
clear parameters;
clear p;

pbase.g=mg;
pbase.pcomp=mpcomp;
pbase.pnest=mpnest;
pbase.kr=mkr;
pbase.ks=mks;
pbase.kl=mkl;
pbase.kw=mkw;

parameters={};

for g=vg
    p=pbase;
    p.g=g;
    p.type='g';
    parameters{end+1}=p;
end

for pcomp=vpcomp
    p=pbase;
    p.pcomp=pcomp;
    p.type='pcomp';
    parameters{end+1}=p;
end

for pnest=vpnest
    p=pbase;
    p.pnest=pnest;
    p.type='pnest';
    parameters{end+1}=p;
end

for kr=vkr
    p=pbase;
    p.kr=kr;
    p.type='kr';
    parameters{end+1}=p;
end

for ks=vks
    p=pbase;
    p.ks=ks;
    p.type='ks';
    parameters{end+1}=p;
end

for kl=vkl
    p=pbase;
    p.kl=kl;
    p.type='kl';
    parameters{end+1}=p;
end

for kw=vkw
    p=pbase;
    p.kw=kw;
    p.type='kw';
    parameters{end+1}=p;
end

parameters_old=parameters;
parameters={};

for k=1:length(parameters_old)
    p=parameters_old{k};
    H=sqrt(p.ks/p.kr);
    if(H>p.g)
        for rep=vrep
            i++;
            indice=sprintf('%.8d',i);

            p.Nh = floor(sqrt(p.ks/p.kr)+.5);
            p.Np = floor(sqrt(p.ks*p.kr)+.5);
            p.Nl = floor(p.kl*p.ks^.63+.5);
            p.Nw = floor(p.Nl*p.kw+.5);

            p.indice=indice;




            parameters{i}=p;
        end
    end
end

save -z netdir/parameters parameters

