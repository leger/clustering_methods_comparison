% This function apply SBM method. wmixnet must be installed.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function c=sbmP(n,bin,g)

    if bin
        W = (n.A>0)*1;
    else
        W = n.A;
    end

    spmfile = sprintf('wmixnet/spm_%s_%i.spm',n.parameters.indice,bin);
    outfile = sprintf('wmixnet/out_%s_%i.m',n.parameters.indice,bin);

    statefile=[spmfile,'.state'];

    funname=sprintf('out_%s_%i',n.parameters.indice,bin);

    write_spm(spmfile,W);

    if bin
        model='bernoulli';
    else
        model='poisson';
    end

    cmd1 = sprintf(...
            'wmixnet -n 1 -i %s -m %s -S %%s -f %s -F octave -o %s -s -Q %i 2>/dev/null', ...
            spmfile,
            model,
            statefile,
            outfile,
            min(n.parameters.g*4,size(n.A,1)));

    %[p,q]=shell_cmd(sprintf(cmd1,'minimal'));
    [p,q]=shell_cmd(sprintf(cmd1,'exhaustive'));
    %printf(cmd1,'exhaustive');
    if(p!=0)
        error('erreur')
    end

    source(outfile);

    fun=str2func(funname);

    r=fun();

    m=imax(r.ICL);
    if(g>0)
        m=g;
    end

    tau=r.tau{m};

    c=arrayfun(@(k)imax(tau(k,:)),(1:length(tau))');

    c=uniformiserclassif(c);

end

function i=imax(v)
    [poub,i]=max(v);
end
