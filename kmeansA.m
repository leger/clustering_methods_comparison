% this function compute a kmeans from the given coordinates usign th given
% number of dimentions. The loop is done by the function kmeansBoucle (in C++).

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function assigment=kmeansA(U,k)

  % first compute distances

  n = size(U,1);

  J=ones(n,1)*(1:n);
  I=J';

  D = zeros(n,n);

  masque = I<J;

  f = @(i,j) norm( U(i,:) - U(j,:) );

  D(masque)=arrayfun(f,I(masque),J(masque));
  
  D=D+D';

  %selection init

  noeuds=zeros(k,1);
  [poub,p] = max(max(D));
  noeuds(1)=p;

  for i=2:k
      [poub,p] = max( min(D(noeuds(1:(i-1)),:)) );
      noeuds(i)=p;
  end

  centroids = U(noeuds,:);

  assigment=kmeansBoucle(U,k,centroids);

end
