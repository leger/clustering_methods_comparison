% This function compute the clustering maximum modularity by spectral analysis
% of the modularity matrix (Newmann method). If the given number of group is
% non-null, the method is stopped at the given number of groups.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 

function classif=mod2(A,k)
    d=sum(A);
    B=A-d'*d/sum(d);
    n=size(A,1);
    [P,V]=(eig(B));
    [vmax,imax]=max(diag(V));

    if(k==0)
        k=Inf;
    end

    partition = { {1:n, B, vmax, P(:,imax)>0} };

    do
        [vmax,iclust]=max(cellfun(@(clust) clust{3}, partition));

        if(vmax<=sqrt(eps(1)))
            break;
        end

        ens = partition{iclust}{1};
        B=partition{iclust}{2};
        classif=partition{iclust}{4};

        ens1=ens(classif);
        ens2=ens(~classif);

        if(length(ens1)>1)
            B1=B(classif,classif);
            B1=B1-diag(diag(B1));
            B1 = B1-diag(sum(B1));
        else
            B1=0;
        end

        if(length(ens2)>1)
            B2=B(~classif,~classif);
            B2=B2-diag(diag(B2));
            B2 = B2-diag(sum(B2));
        else
            B2=0;
        end

        [P1,V1]=eig(B1);
        [vmax1,imax1]=max(diag(V1));
        [P2,V2]=eig(B2);
        [vmax2,imax2]=max(diag(V2));

        clust1 = {ens1, B1, vmax1, P1(:,imax1)>0};
        clust2 = {ens2, B2, vmax2, P2(:,imax2)>0};

        partition={partition{1:iclust-1},clust1,clust2,partition{iclust+1:end}};
    until length(partition)>=k

    classif=zeros(n,1);

    for i=1:length(partition)
        classif(partition{i}{1})=i;
    end



end


