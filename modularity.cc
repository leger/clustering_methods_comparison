/* This file contains a C++ function for GNU Octave. The file must be
 * compiated by mkoctfile provided by Octave.
 *
 * Warning: This file must be linked with igraph library (-ligraph on the
 * command line).
 *
 * Compute the modularity of a given partition.
 */

/* Copyright 2012-2013 INRA, France, Jean-Benoist Leger
 *                                     <jbleger@bordeaux.inra.fr>
 * 
 * This software is a computer program whose purpose is to simulated 
 * ecological bipartite graphs, apply clustering methods on these graphs.
 * 
 * This software is governed by the CeCILL-B  license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/ or redistribute the software under the terms of the 
 * CeCILL-B license. The licence can be found in the LICENCE file in the
 * root directory.
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 * 
 */

#include <iostream>
#include <octave/oct.h>
#include <octave/ov.h>
#include <octave/oct-map.h>
#include <octave/oct-rand.h>
#include <octave/randmtzig.h>

#include <igraph/igraph.h>


DEFUN_DLD(modularity, args, nargout, "Documentation que vous ne verrez jamais")
{
    const Matrix A = args(0).matrix_value();
    const Matrix Classif = args(1).matrix_value();
    
    igraph_matrix_t mA;
    igraph_matrix_init(&mA,A.rows(),A.cols());

    for(int i=0;i<A.rows();i++)
        for(int j=0;j<A.cols();j++)
            *(igraph_matrix_e_ptr(&mA,i,j))=A(i,j);

    igraph_t graph;
    igraph_adjacency(&graph, &mA,IGRAPH_ADJ_UNDIRECTED);


    igraph_vector_t classif;
    igraph_vector_init(&classif,Classif.numel());

    for(int i=1;i<Classif.numel();i++)
        *(igraph_vector_e_ptr(&classif,i)) = Classif(i);

    double modularity;

    igraph_modularity(&graph,&classif,&modularity,NULL);

    igraph_matrix_destroy(&mA);
    igraph_destroy(&graph);
    igraph_vector_destroy(&classif);
    return(octave_value(modularity));

}

