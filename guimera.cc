/* This file contains a C++ function for GNU Octave. The file must be
 * compiated by mkoctfile provided by Octave.
 *
 * Warning: This file must be linked with igraph library in compilation.
 * (-ligraph on the command line).
 *
 * This function apply a Guimera-like algorithm. The algorithm is a SA, but the
 * split inside the SA is not a embeded SA as guimera but a greedy algoritm.
 */

/* Copyright 2012-2013 INRA, France, Jean-Benoist Leger
 *                                     <jbleger@bordeaux.inra.fr>
 * 
 * This software is a computer program whose purpose is to simulated 
 * ecological bipartite graphs, apply clustering methods on these graphs.
 * 
 * This software is governed by the CeCILL-B  license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/ or redistribute the software under the terms of the 
 * CeCILL-B license. The licence can be found in the LICENCE file in the
 * root directory.
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 * 
 */

#include <iostream>
#include <octave/oct.h>
#include <octave/ov.h>
#include <octave/oct-map.h>
#include <octave/oct-rand.h>
#include <octave/randmtzig.h>

#include <igraph/igraph.h>

#define SEUIL 0.0000001

#define KCONSECUTIFSMINI 10
#define VITESSEMINI vitesse
#define TINITMINI(n) (43*pow(n,-1.2))

#define LIMGLOUTON 0

#define TINIT(n) (30*pow(n,-1.27))

double modularity(const Matrix &A,const Matrix &Classif);
//Matrix splitClasse(Matrix Ar);
double splitClasse(Matrix Ar,double);
Matrix splitClasseGlouton(Matrix Ar);
Matrix genAleaPermut(int n);
/*
DEFUN_DLD(wrapperSplitClasse, args, nargout, "")
{
    const Matrix A = args(0).matrix_value();
    const double vitesse = args(1).double_value();
    double R = splitClasse(A,vitesse);
    return(octave_value(R));
}

DEFUN_DLD(wrapperSplitClasseGlouton, args, nargout, "")
{
    const Matrix A = args(0).matrix_value();
    double R = splitClasseGlouton(A);
    return(octave_value(R));
}
*/

DEFUN_DLD(guimera, args, nargout, "Documentation que vous ne verrez jamais")
{
    const Matrix A = args(0).matrix_value();
    const double fa = args(1).double_value();
    const double f=1;
    const int n = A.rows();

    double T=TINIT(n);
    
    Matrix Classif(n,1,1);
    Matrix Classif2;
    Matrix Classes(2,1,1);
    Classes(1)=2;
    Matrix Classes2;

    double lastm;
    double m = modularity(A,Classif);
    int consecutifs = 0;

    // There we go
    int iter=0;
    double ta=1;
    while(consecutifs<1000)
    {
        lastm=m;
        ta*=0.999;
        double u=oct_randu();
        // Mouvement individuel
        if(n*u>2)
        {
            Classif2 = Classif;
            
            int j = n*oct_randu();
            int c = Classes.numel()*oct_randu();
            Classif2(j)=Classes(c);
            double m2 = modularity(A,Classif2);

            if(oct_randu()<exp((m2-m)/T))
            {
                ta+=0.001;
                Classif=Classif2;
                m=m2;
            }
//                else
//                    printf("0");
        }
        else if(n*u>1)
        {
            // Un split
            int c = oct_randu()*Classes.numel();
            int nbc = 0;
            for(int i=0;i<Classif.numel();i++)
                if(Classif(i)==Classes(c))
                    nbc++;


    //          std::cout << Classif.transpose() << std::endl;
    //          std::cout << Classes(c) << " " << nbc << std::endl;
            if(nbc>1)
            {
                // Effective split
                Matrix indices(nbc,1,0);

                {
                    int point=0;
                    for(int i=0;i<Classif.numel();i++)
                        if(Classif(i)==Classes(c))
                        {
                            indices(point)=i;
                            point++;
                        }
                }

                Matrix Ar(nbc,nbc,0);
                for(int i=0;i<nbc;i++)
                    for(int j=0;j<nbc;j++)
                        Ar(i,j) = A(indices(i),indices(j));

    //              std::cout << Ar << std::endl;

                if((Ar.sum().sum())(0)!=0)
                {

                    Matrix Classifr = splitClasseGlouton(Ar);

                    int c2=1;
                    for(int i=0;i<Classes.numel();i++)
                        if(Classes(i)+1>c2)
                            c2=Classes(i)+1;

                    Classif2 = Classif;
                    for(int i=0;i<nbc;i++)
                        if(Classifr(i)==1)
                            Classif2(indices(i))=c2;

                    Classes2 = Matrix(Classes.numel()+1,1,0);
                    Classes2(0)=c2;
                    for(int i=0;i<Classes.numel();i++)
                        Classes2(i+1)=Classes(i);


                    double m2 = modularity(A,Classif2);
                    if(oct_randu()<exp((m2-m)/T))
                    {
                        Classif=Classif2;
                        Classes = Classes2;
                        m=m2;
                        ta+=0.001;
                    }
                }

            }
        }
        else
        {
            // Un merge
            if(Classes.numel()>2)
            {
                int c1=0;
                int c2=0;
                while(c1==c2)
                {
                    c1 = (oct_randu()*Classes.numel());
                    c2 = (oct_randu()*Classes.numel());
                }
                
                Classes2=Matrix(Classes.numel()-1,1,0);

                {
                    int i=0;
                    for(int j=0;i<Classes2.numel();j++)
                    {

                        if(i==c2)
                            i++;
        
                        Classes2(j)=Classes(i);
     
                        i++;
                    }
                }

                Classif2=Classif;

                for(int i=0;i<Classif2.numel();i++)
                    if(Classif2(i)==Classes(c2))
                        Classif2(i)=Classes(c1);

    //                std::cout << Classif.transpose() << std::endl;
    //                std::cout << Classif2.transpose() << std::endl;
    //                std::cout << Classes.transpose() << std::endl;
    //                std::cout << Classes2.transpose() << std::endl;

                double m2 = modularity(A,Classif2);
                if(oct_randu()<exp((m2-m)/T))
                {
                    Classif=Classif2;
                    Classes = Classes2;
                    m=m2;
                    ta+=0.001;
                }


            }
        }
                

        T=T*fa;
        //if(iter%1000==0)
            //printf("%f\t%f\t%d\t%f\n",T,ta,Classes.numel(),m);
        iter++;

        
        if(m-lastm<SEUIL&&lastm-m<SEUIL)
            consecutifs++;
        else
            consecutifs=0;
        

    }

    return(octave_value(Classif));
}

//Matrix splitClasse(Matrix Ar)
Matrix splitClasseGlouton(Matrix Ar)
{
    int n=Ar.rows();
    Matrix Classif(n,1,0);

    for(int i=0;i<n;i++)
        Classif(i) = (oct_randu()<0.5) ? 0 : 1;

    int idem=0;

    double m=modularity(Ar,Classif);
    double mS = m;
    Matrix ClassifS = Classif;
    int dS = 0;

    while(idem<=LIMGLOUTON)
    {
        idem++;
        Matrix ordre = genAleaPermut(n);
        for(int i=0;i<n;i++)
        {
            Classif(ordre(i)) = (Classif(ordre(i))==0) ? 1 : 0;
            m = modularity(Ar,Classif);
            if(m>mS)
            {
                ClassifS=Classif;
                mS=m;
                idem=0;
                dS=0;
            }
            else
            {
                if(dS<LIMGLOUTON)
                {
                    dS++;
                }
                else
                {
                    dS = 0;
                    Classif=ClassifS;
                }
            }
                    
        }

    }


    return(ClassifS);
}

Matrix genAleaPermut(int n)
{
    Matrix P(n,1,0);
    
    for(int i=0;i<n;i++)
        P(i)=i;

    for(int i=0;i<n;i++)
    {
        int k=i+oct_randu()*(n-i);
        double prov = P(i);
        P(i)=P(k);
        P(k)=prov;
    }

    return(P);
}



double splitClasse(Matrix Ar,double vitesse)
{
    int n=Ar.rows();
    Matrix Classif(n,1,0);
    Classif(0)=1;
    int nb0=n-1;
    int nb1=1;
    int nb02, nb12;
    Matrix Classif2;
    double m = modularity(Ar,Classif);
    double T=TINITMINI(n);
    int consecutifs=0;
    double lastm = m;
    double ta=1;

    int i=0;
    while(consecutifs<KCONSECUTIFSMINI*n)
    {
        ta*=0.999;
        int k = oct_randu()*n;
        Classif2 = Classif;
        bool dontdothat = false;
        if(Classif(k)==0)
        {
            if(nb0<2)
                dontdothat=true;
            else
            {
                Classif2(k)=1;
                nb12=nb1+1;
                nb02=nb0-1;
            }
        }
        else
        {
            if(nb1<2)
                dontdothat=true;
            else
            {
                Classif2(k)=0;
                nb12=nb1-1;
                nb02=nb0+1;
            }
        }
        
        if(!dontdothat)
        {
            double m2 = modularity(Ar,Classif2);
            if(oct_randu()<exp((m2-m)/T))
            {
                Classif=Classif2;
                m=m2;
                nb0=nb02;
                nb1=nb12;
                ta+=0.001;
            }
        }

        if(m-lastm<SEUIL&&lastm-m<SEUIL)
            consecutifs++;
        else
            consecutifs=0;
        
        lastm=m;
        
        //if(i%200==0)
        //    printf("\t\t\t->\t%d\t%d\t%f\t%f\t%f\n",i,consecutifs,T,ta,m);

        T*=VITESSEMINI;
        i++;
    }

        


    //return Classif;
    return m;
}

    




double modularity(const Matrix &A,const Matrix &Classif)
{
    igraph_matrix_t mA;
    igraph_matrix_init(&mA,A.rows(),A.cols());

    for(int i=0;i<A.rows();i++)
        for(int j=0;j<A.cols();j++)
            *(igraph_matrix_e_ptr(&mA,i,j))=A(i,j);

    igraph_t graph;
    igraph_adjacency(&graph, &mA,IGRAPH_ADJ_UNDIRECTED);


    igraph_vector_t classif;
    igraph_vector_init(&classif,Classif.numel());

    for(int i=1;i<Classif.numel();i++)
        *(igraph_vector_e_ptr(&classif,i)) = Classif(i);

    double modularity;

    igraph_modularity(&graph,&classif,&modularity,NULL);

    igraph_matrix_destroy(&mA);
    igraph_destroy(&graph);
    igraph_vector_destroy(&classif);
    return(modularity);
}

