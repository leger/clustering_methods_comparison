% This function apply edge-betweeness on a given graph. The function ebC is
% used. If the given number of groups is null, the maximum of modularity is
% choosen.


% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function classif=eb(net,bin,k)

  if(bin)
      A=1*(net.A>0);
      nlinks = net.parameters.Nl;
  else
      A=net.A;
      nlinks = net.parameters.Nw;
  end
  
%  estimated_time = 3.425e-8 * (net.parameters.Nh+net.parameters.Np)*nlinks**2 + exp(-5.68+.5*log(net.parameters.Np+net.parameters.Nh));

%  if(estimated_time>25.5)
%      classif=[];
%  else

      %t=cumtime();

      Merges = ebC(A);
      n=size(A,1);

      classif = (1:n)' - 1;

      newcluster=n;
      classifs=cell(size(Merges,1),1);
      for i=1:size(Merges,1)
          classif(classif==Merges(i,1)) = newcluster;
          classif(classif==Merges(i,2)) = newcluster;
          newcluster++;
          classifs{i}=classif;
      end


      if(k>0)
          if(n-k>length(classifs))
              classif=classifs{end};
          else
              classif=classifs{n-k};
          end
      else
          %mods = cellfun(@(c)modularity(A,c),classifs);
          %[poubelle,imax]=max(mods);
          %classif=classifs{imax};

          r=goldenmaxentiers(@(i)-modularity(A,classifs{i}),1,length(classifs));
          classif=classifs{r};


      end



      classif=uniformiserclassif(classif);
      

  end
  %t=cumtime()-t;
  %fprintf(stderr,"%f\t%f\n",estimated_time,t);

%end

      
