/* This file contains a C++ function for GNU Octave. The file must be
 * compiated by mkoctfile provided by Octave.
 *
 * Warning this function must be linked with the igraph library (-ligraph in the
 * compilation command).
 *
 * The main purpose of this file is compute the edge-betweeness by the igraph
 * library.
 */

/* Copyright 2012-2013 INRA, France, Jean-Benoist Leger
 *                                     <jbleger@bordeaux.inra.fr>
 * 
 * This software is a computer program whose purpose is to simulated 
 * ecological bipartite graphs, apply clustering methods on these graphs.
 * 
 * This software is governed by the CeCILL-B  license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/ or redistribute the software under the terms of the 
 * CeCILL-B license. The licence can be found in the LICENCE file in the
 * root directory.
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 * 
 */
#include <iostream>
#include <octave/oct.h>
#include <octave/ov.h>
#include <octave/oct-map.h>
#include <octave/oct-rand.h>
#include <octave/randmtzig.h>

#include <igraph/igraph.h>

DEFUN_DLD(ebC, args, nargout, "Documentation que vous ne verrez jamais")
{
    const Matrix A = args(0).matrix_value();

    igraph_matrix_t mA;
    igraph_matrix_init(&mA,A.rows(),A.cols());

    for(int i=0;i<A.rows();i++)
        for(int j=0;j<A.cols();j++)
            *(igraph_matrix_e_ptr(&mA,i,j))=A(i,j);

    igraph_t graph;
    igraph_adjacency(&graph, &mA,IGRAPH_ADJ_UNDIRECTED);

    igraph_vector_t result;
    igraph_vector_init(&result,0);
    igraph_matrix_t merges;
    igraph_matrix_init(&merges,0,0);
    igraph_community_edge_betweenness(&graph,&result,
            NULL,&merges,NULL,0);
/*
    Matrix Result(igraph_vector_size(&result),1,0);

    for(int i=0;i<Result.rows();i++)
        Result(i,0) = *(igraph_vector_e_ptr(&result,i));
*/

    Matrix Merges(igraph_matrix_nrow(&merges),igraph_matrix_ncol(&merges),0);

    for(int i=0;i<Merges.rows();i++)
        for(int j=0;j<Merges.cols();j++)
            Merges(i,j) = *(igraph_matrix_e_ptr(&merges,i,j));



    return(octave_value(Merges));
}
