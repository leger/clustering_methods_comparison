This directory contains all the necessary code to simulate networks

This code is for GNU Octave
===========================

This is designed for GNU Octave. Most file are in m-code. Some file are C++ code
for GNU Octave, these files must be compiled for GNU Octave whith mkoctfile
provided by Octave. Most of C++ file need to be linked with the igraph library.

Examples:

 * `mkoctfile kmeansBoucle.cc`

 * `mkoctfile -ligraph guimera.cc`

Please also note a intensive usage of functions `parcellfun` and `pararrayfun`
which are provided by the package `octave-forge-general`. This package is
provided by most Linux distribution as the package `octave-general`, or it can
be found in the `octave-forge` repository. However the occurence of `parcellfun`
and `pararrayfun` can be substituted by the classical `cellfun` and `arrayfun`
(the first argument must be removed, see help) with the loose of the benefit of
parrallism.


Usage
=====

To generate parameters
----------------------

Setting the parameters range, the parameters structure must be generated. This
parameters structure contains the association of parameters for each network.

 * `write_cst_parameters.m` Used to write the parameter structure for all
   networks of the central point. Please note that each parameter set is the
   same, because we are in the central point.

 * `write_parameters.m` Used to write the parameters structure for varing one by
   one parameters in parameter range with replicates.


To generate networks
--------------------

Once the parameters structures are generated, the networks can be generated
using the parameter structure.

 * `generate_networks.m`


Generated parameters and networks
---------------------------------

In directory `netdir/`, the

 * `parameters` and `cst_parameters` are the used parameters. cst for the central
   point.

 * `networks` and `cst_networks` are the used networks. cst for the central point.

All are provided in GNU Octave format, loadable in GNU Octave with the function
load. There are also provided in R data single object format (these files have
the extention .rds), which are loadable in GNU R with the function `readRDS`.

For further usage, to convert GNU Octave object to GNU R object see `to_R.m`

If you want to run other clustering methods on the same networks, you have to
use this data.


To compute all clusterings
--------------------------

 * `calc.m` Which compute all clustering for networks generated with varying
   parameters , the real number of groups is known.

 * `blind.m` Which compute all clustering for networks generated with varying
   parameters , the real number of groups is unknown.

 * `cst_calc.m` Which compute all clustering for networks generated with cst
   parameters , the real number of groups is known.

 * `cst_blind.m` Which compute all clustering for networks generated with cst
   parameters , the real number of groups is unknown.


Methods to compute clustering
-----------------------------

 * `eb.m` Edge-betweeness implementation using the `igraph` library. [The EB
   method in the article use this function].

 * `mcl.m` MCL implementation. This function provide MCL with the weight of
   self-loops as argument. [The MCL and MCL_{1/10} methods in the article use
   this function].

 * `mod2.m` Modularity maximization, with eigen analysis of the modularity
   matrix. Newmann algorithm. [The Mod method in the article use this function].

 * `sbmP.m` SBM Method. This is only a wrapper of the wmixnet program. See
   http://arxiv.org/abs/1402.3410. [The SBM method in the article use this
   function].

 * `sc.m` Spectral Clustering implementation. This function provide a general SC
   with choice of normalization, and transformation of eigenvalues. [The ASC and
   NSC method in the article use this function]

 * `guimera.cc` Guimera like implementation. See the comments in the file for
   the difference with the original Guimera algorithm. This function is not used
   in the main comparison, but used to compare the Newmann greedy modularity
   maximization to the Guimera (in fact Guimera-like) method.


To compute score
----------------

Once the clustering is computed, scores of clustering, /i.e./ measure between
obtained partition and partition used for simulation should be computed.

 * `score.m` Compute all scores used in the article (ARI on both trophic level,
   adjusted R^2 and ratio of groups number.


Computed scores
---------------

Computed scores can be found in `allres` object in `allres` file in `netdir/`
directory. The object is also available in R data single object format
(extention .rds readable by function `readRDS` in GNU R).

Tools
-----

For various previous functions, some functions are used. There are given here.

 * `ari.m` Computation of the ARI. Usefull for scores.
 
 * `cumtime.m` Cumulated time of the process and dead children, usefull for
   timing.

 * `doallres.m` Merging all results in one structure.

 * `ebC.cc` The part of the EB method wich is linked with igraph. This function
   is called by function `eb`.

 * `generate1network.m` Generation of one network given one parameter set.

 * `kmeansA.m` Implementation of k-means algorithm, usefull for SC.

 * `kmeansBoucle.cc` The loop of the kmeans, implemented in C++. This function
   is called by `kmeansA`.

 * `loghist.m` Plot a historgam with x-axis in log-scale. Usefull for analysis.

 * `modularity.cc` Computation of the modularity. Linked with `igraph` library.
   Usefull for `eb` with unknown number of groups.

 * `to_R.m` Function which write in a file sourcable by R a Octave object.

 * `to_R_str.m` Internal function with recursive call used by `to_R.m`

 * `uniformiserclassif.m` Function to renumbering cluster index in a unique view.

 * `write_spm.m` Function used to write a network in a `spm` file. Usefull for
   `sbmP`.
