% This script launch all clustering methods on graphs of the central point. For
% a known number of groups.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


allres=cell(7,1);

% sc
t=cumtime();
allres{1}.cfw=parcellfun(np,@(n) sc(n.A,n.parameters.g,1,1),cst_networks,'UniformOutput',false);
allres{1}.cfwt=cumtime()-t;
t=cumtime();
allres{2}.cfw=parcellfun(np,@(n) sc(n.A,2*n.parameters.g,2,0),cst_networks,'UniformOutput',false);
allres{2}.cfwt=cumtime()-t;

t=cumtime();
allres{1}.cfb=parcellfun(np,@(n) sc(1*(n.A>0),n.parameters.g,1,1),cst_networks,'UniformOutput',false);
allres{1}.cfbt=cumtime()-t;
t=cumtime();
allres{2}.cfb=parcellfun(np,@(n) sc(1*(n.A>0),2*n.parameters.g,2,0),cst_networks,'UniformOutput',false);
allres{2}.cfbt=cumtime()-t;

% mcl
t=cumtime();
allres{3}.cfw=parcellfun(np,@(n) mcl(n.A,1,n.parameters.g),cst_networks,'UniformOutput',false);
allres{3}.cfwt=cumtime()-t;
t=cumtime();
allres{4}.cfw=parcellfun(np,@(n) mcl(n.A,.1,2*n.parameters.g),cst_networks,'UniformOutput',false);
allres{4}.cfwt=cumtime()-t;

t=cumtime();
allres{3}.cfb=parcellfun(np,@(n) mcl(1*(n.A>0),1,n.parameters.g),cst_networks,'UniformOutput',false);
allres{3}.cfbt=cumtime()-t;
t=cumtime();
allres{4}.cfb=parcellfun(np,@(n) mcl(1*(n.A>0),.1,2*n.parameters.g),cst_networks,'UniformOutput',false);
allres{4}.cfbt=cumtime()-t;

% eb
t=cumtime();
allres{5}.cfb=parcellfun(np,@(n) eb(n,1,n.parameters.g),cst_networks,'UniformOutput',false);
allres{5}.cfbt=cumtime()-t;
t=cumtime();
allres{5}.cfw=parcellfun(np,@(n) eb(n,0,n.parameters.g),cst_networks,'UniformOutput',false);
allres{5}.cfwt=cumtime()-t;

% mod
t=cumtime();
allres{6}.cfw=parcellfun(np,@(n) mod2(n.A,n.parameters.g),cst_networks,'UniformOutput',false);
allres{6}.cfwt=cumtime()-t;
t=cumtime();
allres{6}.cfb=parcellfun(np,@(n) mod2(1*(n.A>0),n.parameters.g),cst_networks,'UniformOutput',false);
allres{6}.cfbt=cumtime()-t;

% sbm
t=cumtime();
allres{7}.cfb=parcellfun(np,@(n) sbmP(n,1,2*n.parameters.g),cst_networks,'UniformOutput',false);
allres{7}.cfbt=cumtime()-t;
t=cumtime();
allres{7}.cfw=parcellfun(np,@(n) sbmP(n,0,2*n.parameters.g),cst_networks,'UniformOutput',false);
allres{7}.cfwt=cumtime()-t;

save -z netdir/cst_calc_results
