% This function do a simple (non-guaranted) optimization to optimized a function
% on integers.


% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function r=goldenmaxentiers(f,xa,xd)
    % minimization algo
    phi = (1+sqrt(5))/2;
    xb=floor(xa+1/(1+phi)*(xd-xa)+.5);
    xc=floor(xa+phi/(1+phi)*(xd-xa)+.5);

    bb=0;
    bc=0;

    neval=0;

    while(xb!=xc)
        if(bb==0)
            yb=f(xb);
            bb=1;
            neval++;
        end

        if(bc==0)
            yc=f(xc);
            bc=1;
            neval++;
        end

        if(yb<yc)
            xd=xc;
            xc=xb;
            yc=yb;
            xb=floor(xa+1/(1+phi)*(xd-xa)+.5);
            bb=0;
        else
            xa=xb;
            xb=xc;
            yb=yc;
            xc=floor(xa+phi/(1+phi)*(xd-xa)+.5);
            bc=0;
        end
    end

    r=xb;
end


