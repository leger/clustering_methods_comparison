% This function is a GNU Octave function to compute Adjsted Rand-index
% from to classification provided as arguments.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 

function r=ari(c1,c2)
  c1=uniformiserclassif(c1);
  c2=uniformiserclassif(c2);

  N1=max(c1);
  N2=max(c1);

  n= @(i,j) sum((c1==i).*(c2==j));
  a= @(i) sum(c1==i);
  b= @(i) sum(c2==i);

  c2pn = @(k) k*(k-1)/2;

  [I,J]=meshgrid(1:N1,1:N2);

  tn=sum(arrayfun(@(i,j) c2pn(n(i,j)),I,J)(:));

  ta=sum(arrayfun(@(i) c2pn(a(i)),1:N1));
  tb=sum(arrayfun(@(i) c2pn(b(i)),1:N2));

  K=c2pn(length(c1));

  numerateur = (tn-(ta*tb)/K);
  denominateur = (.5*(ta+tb)-(ta*tb)/K);

  if numerateur==0 && denominateur==0
      r=1;
  else
      r=numerateur/denominateur;
  end


end



    
