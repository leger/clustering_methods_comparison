/* This file contains a C++ function for GNU Octave. The file must be
 * compiated by mkoctfile provided by Octave.
 *
 * This function is a loop of a kmeans. This is design to be used in the
 * function kmeansA.
 */

/* Copyright 2012-2013 INRA, France, Jean-Benoist Leger
 *                                     <jbleger@bordeaux.inra.fr>
 * 
 * This software is a computer program whose purpose is to simulated 
 * ecological bipartite graphs, apply clustering methods on these graphs.
 * 
 * This software is governed by the CeCILL-B  license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/ or redistribute the software under the terms of the 
 * CeCILL-B license. The licence can be found in the LICENCE file in the
 * root directory.
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 * 
 */

#include <iostream>
#include <octave/oct.h>
#include <octave/ov.h>
#include <octave/oct-map.h>
#include <octave/oct-rand.h>
#include <octave/oct-norm.h>
#include <octave/randmtzig.h>
//#include <ov-re-mat.h>
//#include <octave/boolMatrix.h>


DEFUN_DLD(kmeansBoucle, args, nargout, "Documentation que vous ne verrez jamais")
{
    const Matrix X = args(0).matrix_value();
    const int nbClusters = args(1).double_value();
    Matrix centroids = args(2).matrix_value();

    const int n = X.rows();
    const int dim = X.columns();

    ColumnVector assigment(n,0);
    RowVector jmed(dim,0);
    double inertie=-1;

    while(true)
    {
        ColumnVector oldAssigment = assigment;
        double oldinertie=inertie;
        inertie=0;

        // Compute assigments

        for(int i=0; i<n; i++)
        {
            assigment(i)=0;
            //double normmin = (X.row(i)-centroids.row(0)).norm((double)2);
            Matrix vdiff = X.row(i)-centroids.row(0);
            double normmin = (vdiff*vdiff.transpose())(0);
            for(int j=1;j<nbClusters;j++)
            {
                vdiff = X.row(i)-centroids.row(j);
                double normj = (vdiff*vdiff.transpose())(0);
                if(normj<normmin)
                {
                    assigment(i)=j;
                    normmin=normj;
                }
            }
            inertie+=normmin;
        }

//        std::cerr << assigment << std::endl << std::endl;
        if(abs(oldinertie-inertie)<1e-10)
            break;



        // Compute centroids

        for(int j=0;j<nbClusters;j++)
            for(int k=0;k<dim;k++)
                centroids(j,k)=0;

        ColumnVector count(nbClusters,0); 
        for(int i=0;i<n;i++)
        {
            int j = assigment(i);
            count(j)++;
            for(int k=0;k<dim;k++)
                centroids(j,k)+=X(i,k);
        }

        for(int j=0;j<nbClusters;j++)
            for(int k=0;k<dim;k++)
                centroids(j,k)/=count(j);



    }


    octave_value_list r;
    r.append(octave_value(assigment));
    r.append(octave_value(inertie));
    return(r);
}
