% This function apply mcl method with the given selfloop. The number of groups
% can be given, parameters are adjusted to found near the correct number of
% groups. If the given number of groups is 0, the number of groups is choosen by
% the method.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function assigment=mcl(W,sl,k)
    if(k==0)
        assigment = mcl_internal(W,sl,2.56);
    else
        factor=1;
        do
            factor+=.1;
            assigment=mcl_internal(W,sl,factor);
            if(factor>50)
                break;
            end
        until length(unique(assigment))>=k
    end
end


function assigment=mcl_internal(W,sl,r)
  W = W+sl*diag(sum(W,2)./sum(W~=0,2));
  D = diag( sum(W,2) );

  T=W*inv(D);

  e=2;

  do
    aT=T;
    T=T^e;
    T=T.^r;
    T=T./(ones(size(T,1),1)*sum(T,1));
  until max(abs(aT-T)(:))<2*eps(max(abs(T(:))));


  assigment=arrayfun(@(i) imax(T(:,i)),(1:size(W,1))(:));

  assigment = uniformiserclassif(assigment);

endfunction

function p=imax(v)
  [poub,p]=max(v);
endfunction
