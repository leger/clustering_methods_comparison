% This function generate a network with the given parameters.

% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


function network = generate1network(parameters)
  % network = generate1network (parameters)
  % Generate one network
  %
  % parameters is a struct with following fields :
  %   Np : number of parasites
  %   Nh : number of hosts
  %   g  : number of groups
  %   pnest : probablilty of use the nested rule
  %   pcomp : probablilty of use the comartimentalization rule
  %   Nl  : number of links
  %   Nw  : sum of weights 
  %
  % network is a struc with following fields
  %   parameters : paramters struc (see below)
  %   Pp, Ph     : 2-power-law used for nestedness of parasites and hosts
  %   Gp, Gh     : groups of hosts and parasites

  Np = parameters.Np;
  Nh = parameters.Nh;
  g  = parameters.g;
  pnest = parameters.pnest;
  pcomp = parameters.pcomp;
  Nl = parameters.Nl;
  Nt = parameters.Nw;

  % usefull functions
  normalize = @(x) x / sum (x);
   % choice randomly a index in I
   random_index_unif = @(I) I(floor(rand(1)*length(I)+1));
   % choice randomly a index in I with assciocated non nomalizated proba p
   random_index = @(I,p) I(find(rand(1)<cumsum(normalize(p)))(1));

%   loop=1;
%   while loop
        % adjacency matrix
        Ar = zeros(Nh,Np);
      
        % power law distribution (degree 2) for nestedness
        Pp = 1./(1-rand(Np,1));
        Ph = 1./(1-rand(Nh,1));
      
        % groups
        do
          Gp = floor(g*sort(rand(Np,1)));
        until length(unique(Gp))==g

        do
          Gh = floor(g*sort(rand(Nh,1)));
        until length(unique(Gh))==g
      
        % links generation
        while(sum(Ar(:))<Nt)
          
          if(rand(1)<.5)
            % first = host, second = parasite
            
            % first
            if(rand(1)<pnest)
              interactH = random_index(1:Nh,Ph);
            else
              interactH = random_index_unif(1:Nh);
            end
            
            % possible second
            if(rand(1)<pcomp)
              possibleP = find(Gp==Gh(interactH));
              if(length(possibleP)==0)
                continue;
              end
            else
              possibleP = 1:Np;
            end
      
            % second
            if(rand(1)<pnest)
              interactP = random_index(possibleP,Pp(possibleP));
            else
              interactP = random_index_unif(possibleP);
            end
      
          else
            % first = parasite, second = host
      
            % fist
            if(rand(1)<pnest)
              interactP = random_index(1:Np,Pp);
            else
              interactP = random_index_unif(1:Np);
            end
      
            % possible second
            if(rand(1)<pcomp)
              possibleH = find(Gh==Gp(interactP));
              if(length(possibleH)==0)
                continue;
              end
            else
              possibleH = 1:Nh;
            end
            
            % second
            if(rand(1)<pnest)
              interactH = random_index(possibleH,Ph(possibleH));
            else
              interactH = random_index_unif(possibleH);
            end
          end
      
          % We do the links
          if(sum(sum(Ar>0))<Nl)
            Ar(interactH,interactP) += 1;
          elseif Ar(interactH,interactP)>0
            Ar(interactH,interactP) += 1;
          end
        end
      
        % A is symetric

%        loop = ~(all(sum(Ar,2)>0)&&all(sum(Ar,1)>0));
%    end

    idxl=sum(Ar,2)>0;
    idxc=sum(Ar,1)>0;
    Ar=Ar(idxl,idxc);

    Nh=size(Ar,1);
    Np=size(Ar,2);

    A = [zeros(Nh),Ar; Ar',zeros(Np)];

    parameters.Nh=Nh;
    parameters.Np=Np;
    
  network.parameters = parameters;
  network.Pp = Pp(idxc);
  network.Ph = Ph(idxl);
  network.Gp = Gp(idxc);
  network.Gh = Gh(idxl);

  network.A = A;
  network.Ar = Ar;
  

end







