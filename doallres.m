% This script merge results from calc/blind/cst_calc/cst_blind scripts.


% Copyright 2012-2013 INRA, France, Jean-Benoist Leger
%                                     <jbleger@bordeaux.inra.fr>
% 
% This software is a computer program whose purpose is to simulated 
% ecological bipartite graphs, apply clustering methods on these graphs.
% 
% This software is governed by the CeCILL-B  license under French law and
% abiding by the rules of distribution of free software. You can use, 
% modify and/ or redistribute the software under the terms of the 
% CeCILL-B license. The licence can be found in the LICENCE file in the
% root directory.
% 
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty  and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability. 
% 
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or 
% data to be ensured and, more generally, to use and operate it in the 
% same conditions as regards security. 
% 
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL-B license and that you accept its terms.
% 


allres=cell(7,1);

allres{1}.legend='NSC';
allres{1}.shs=0;
allres{2}.legend='ASC';
allres{2}.shs=1;
allres{3}.legend='MCL';
allres{3}.shs=0;
allres{4}.legend='MCL_{1/10}';
allres{4}.shs=1;
allres{5}.legend='EB';
allres{5}.shs=0;
allres{6}.legend='Mod';
allres{6}.shs=0;
allres{7}.legend='SBM';
allres{7}.shs=1;

for i=1:7
    allres{i}.shortlegend = allres{i}.legend;
end

function a1=fusionner(a1,a2)
    champs={'cfw','cfwt','cfb','cfbt','cBw','cBwt','cBb','cBbt','fw','fb','Bw','Bb'};
    for i=1:7
        for nc=1:length(champs)
            if length(a2{i})>0
                if any(cellfun(@(x)strcmp(champs{nc},x),fieldnames(a2{i}))) 
                    if any(cellfun(@(x)strcmp(champs{nc},x),fieldnames(a1{i})))
                        r1=getfield(a1{i},champs{nc});
                        r2=getfield(a2{i},champs{nc});
                        for k=1:max(length(r1),length(r2))
                            if length(r2{k})>0
                                r1{k}=r2{k};
                            end
                        end
                        a1{i}=setfield(a1{i},champs{nc},r1);
                    else
                        a1{i}=setfield(a1{i},champs{nc},getfield(a2{i},champs{nc}));
                    end
                end
            end
        end
    end
end

allres=fusionner(allres,load('netdir/calc_results').allres);
allres=fusionner(allres,load('netdir/blind_results').allres);
allres=fusionner(allres,load('netdir/cst_calc_results').allres);
allres=fusionner(allres,load('netdir/cst_blind_results').allres);

allres = allres([2,7,4,3,1,6,5]);

save -z netdir/allres allres
